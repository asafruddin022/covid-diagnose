<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'Home | Covid Diagnose'
        ];
        return view('pages/home', $data);
    }
}
