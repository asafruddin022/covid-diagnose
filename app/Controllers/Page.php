<?php

namespace App\Controllers;

class Page extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'Home | Covid Diagnose'
        ];
        return view('pages/home', $data);
    }

    public function diagnose()
    {
        $data = [
            'title' => 'Self Diagnose'
        ];

        return view('pages/self_diagnose', $data);
    }

    public function expert()
    {
        $data = [
            'title' => 'Expert'
        ];

        return view('pages/expert', $data);
    }

    public function question()
    {
        $data = [
            'title' => 'Question Self Diagnose'
        ];
        return view('Pages/question_self_diagnose', $data);
    }

    public function result()
    {
        $data = [
            'title' => 'Result of Diagnose'
        ];
        return view('Pages/result_diagnose', $data);
    }

    public function expert_logined()
    {
        $data = [
            'title' => 'Pakar'
        ];
        return view('Pages/expert_logined', $data);
    }
}
