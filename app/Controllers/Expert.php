<?php

namespace App\Controllers;

class Expert extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'Expert | Covid Diagnose'
        ];
        return view('pages/indication_parameters', $data);
    }
}
