<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">Diagnosis Dini Covid19</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="d-flex">
            <a href="/pages/expert" class="btn btn-dark" style="margin-right: 10px;">Pakar</a>
            <a href="/pages/self_diagnose" class="btn btn-outline-primary">Self Diagnose</a>
        </div>
    </div>
</nav>