<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<div class="container">
    <div class="row">
        <div class="col">
            <div class="row g-3 mx-5">
                <h1 class="text-center">Diagnosa Sendiri</h1>
                <h3 class="text-center fs-3 fw-lighter">Masukkan data anda terlebih dahulu</h3>
            </div>
            <form class="row g-3 mx-5">
                <div class="col-12">
                    <label for="inputEmail4" class="form-label">Nama Lengkap</label>
                    <input type="email" class="form-control" id="inputEmail4">
                </div>
                <div class="col-12">
                    <label for="inputPassword4" class="form-label">Jenis Kelamin</label>
                    <input type="password" class="form-control" id="inputPassword4">
                </div>
                <div class="col-12">
                    <label for="inputAddress" class="form-label">Alamat</label>
                    <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                </div>
                <div class="d-grid gap-2 col-6 mx-auto">
                    <!-- <button type="submit" class="btn btn-primary" onclick="onSubmit()">Simpan</button> -->
                    <a type="submit" class="btn btn-primary" href="/pages/question">Simpan</a>
                    <p class="text-center">Simpan data untuk melanjutkan ketahap selanjutnya</p>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function onSubmit() {
        window.location.href = "/pages/question"
    }
</script>

<?= $this->endSection(); ?>