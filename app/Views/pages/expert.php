<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<div class="container d-flex justify-content-center">
    <div class="row justify-content-center">
        <div class="col-10 align-items-center">
            <div class="row g-3 mb-5">
                <h1 class="text-center">Login Pakar</h1>
            </div>
            <form class="row mb-3">
                <div class="row mb-3">
                    <label for="inputEmail3" class="col-sm-3 col-form-label">Nama Pengguna</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" id="inputEmail3">
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="inputPassword3" class="col-sm-3 col-form-label">Kata Sandi</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" id="inputPassword3">
                    </div>
                </div>
                <div class="d-grid gap-2 col-6 mx-auto">
                    <!-- <button type="submit" class="btn btn-primary" onclick="onSubmit()">Masuk</button> -->
                    <a type="submit" class="btn btn-primary" href="/pages/expert_logined">Masuk</a>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function onSubmit() {
        window.location.href = "/pages/question"
    }
</script>

<?= $this->endSection(); ?>