<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<div class="container">
    <div class="row">
        <div class="col">
            <div class="row g-3 mx-5">
                <h1 class="text-center">Diagnosa Mandiri</h1>
            </div>
            <div class="row g-3 mx-5 mt-5">
                <h4 class="text-center fw-lighter">Apakah mengalami bersin dan batuk?</h3>
            </div>
            <div class="row mt-5">
                <div class="d-grid gap-2 col-3 mx-auto">
                    <a href="/pages/result" class="btn btn-primary">Ya</a>
                </div>
                <div class="d-grid gap-2 col-3 mx-auto">
                    <a href="" class="btn btn-danger">Tidak</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>