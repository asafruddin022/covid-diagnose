<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<!-- <div class="collapse" id="navbarToggleExternalContent">
    <div class="bg-dark p-4">
        <h5 class="text-white h4">Collapsed content</h5>
        <span class="text-muted">Toggleable via the navbar brand.</span>
    </div>
</div> -->


<div class="container-fluid">
    <div class="row flex-nowrap">
        <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-light">
            <div class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">
                <a href="/" class="d-flex align-items-center pb-3 mb-md-0 me-md-auto text-black text-decoration-none">
                    <span class="fs-3 fw-normal d-none d-sm-inline">Pakar</span>
                </a>
                <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start" id="menu">
                    <li class="nav-item">
                        <a href="#" class="nav-link align-middle px-0">
                            <i class="fs-4 align-middle bi-sliders"></i> <span class="ms-1 align-middle d-none d-sm-inline">Parameter Gejala</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link align-middle px-0">
                            <i class="fs-4 align-middle bi-activity"></i> <span class="ms-1 align-middle d-none d-sm-inline">Data Gejala</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link align-middle px-0">
                            <i class="fs-4 align-middle bi-list-check "></i> <span class="ms-1 align-middle d-none d-sm-inline">Data Rule</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link align-middle px-0">
                            <i class="fs-4 align-middle bi-people"></i> <span class="ms-1 align-middle d-none d-sm-inline">Data Pengguna</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="nav-link px-0 align-middle">
                            <i class="fs-4 align-middle bi-box-arrow-right"></i> <span class="ms-1 align-middle d-none d-sm-inline">Logout</span> </a>
                    </li>
                </ul>
            </div>
        </div>
        <div>
            <h2>Parameter Gejala</h2>

            <a href="#" class="btn btn-primary">
                <i class="fs-4 align-middle bi-plus"></i> <span class="ms-1 align-middle d-none d-sm-inline">Parameter Gejala</span>
            </a>

            <table class="table mt-5">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Kode Parameter</th>
                        <th scope="col">Parameter</th>
                        <th scope="col">Tindakan</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td colspan="2">Larry the Bird</td>
                        <td>@twitter</td>
                    </tr> -->
                </tbody>
            </table>
        </div>

    </div>
</div>



<?= $this->endSection(); ?>