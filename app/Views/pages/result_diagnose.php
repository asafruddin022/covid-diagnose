<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<div class="container">
    <div class="row">
        <div class="col">
            <div class="row g-3 mx-5">
                <h1 class="text-center">Hasil Diagnosa</h1>
            </div>
            <div class="row g-3 mx-5 mt-5">
                <h4 class="text-center fw-lighter">Halaman ini menampilkan hasil diagnosa berdasarkan parameter yang anda alami :</h3>
            </div>

            <div class="border border-dark rounded-3 py-5 px-5">
                <div class="col">
                    <div class="row">
                        <h6 class="fw-light">Tanggal</h5>
                    </div>
                    <div class="row">
                        <h6 class="fw-light">Nama</h5>
                    </div>
                    <div class="row">
                        <h6 class="fw-light">Jenis Kelamin</h5>
                    </div>
                    <div class="row">
                        <h6 class="fw-light">Alamat</h5>
                    </div>
                </div>
            </div>

            <h5 class="text-decoration-underline mt-5">Hasil Diagnosa</h5>
            <h6 class="fw-light">Gejala yang dialami</h6>

            <div class="border border-dark rounded-3 py-5 px-5">
                <h5>Gejala Ringan</h5>
            </div>

            <div class="row mt-5">
                <div class="col-2">
                    <h5>Saran atau Solusi</h5>
                </div>
                <div class="col-10">
                    <p>Tidak keluar rumah, menjaga pola makan</p>
                </div>
            </div>

        </div>
    </div>
</div>

<?= $this->endSection(); ?>